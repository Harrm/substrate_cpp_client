//
// Created by Harrm on 07.03.2021.
//

#ifndef THESISCPPCLIENT_SUBSTRATE_CLIENT_H
#define THESISCPPCLIENT_SUBSTRATE_CLIENT_H

#include <scale/scale.hpp>

#include "types.h"

class WsClient;
namespace jsonrpc {
    class Client;
}

class SubstrateClient {
public:
    SubstrateClient(std::shared_ptr<WsClient> ws_client, std::shared_ptr<jsonrpc::Client> jrpc_client);

    Metadata get_metadata() const;

    Nonce get_nonce() const;

    BlockHash get_genesis_hash() const;

    void submit_encoded_extrinsic(std::vector<uint8_t> enc_ext) const;

    void submit_signed_extrinsic(std::vector<uint8_t> encoded_call, MultiAddress address) const;
    void submit_unsigned_extrinsic(std::vector<uint8_t> encoded_call, MultiAddress address) const;

    Sr25519Signature sign_extrinsic(std::array<uint8_t, SR25519_SEED_SIZE> seed, std::vector<uint8_t> payload) const;

    uint8_t get_module_idx(std::string_view module_name) const;

    uint8_t get_call_idx(std::string_view module_name, std::string_view call_name) const;

    std::string get_actor(uint32_t id) const;

private:
    std::shared_ptr<WsClient> ws_client;
    std::shared_ptr<jsonrpc::Client> jrpc_client;
};


#endif //THESISCPPCLIENT_SUBSTRATE_CLIENT_H
