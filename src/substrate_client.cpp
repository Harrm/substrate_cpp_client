//
// Created by Harrm on 07.03.2021.
//

#include "substrate_client.h"

#include <jsonrpc-lean/client.h>

#include "ws_client.h"
#include "types.h"

namespace {
    const char *kBlack = "\u001b[30m";
    const char *kRed = "\u001b[31m";
    const char *kGreen = "\u001b[32m";
    const char *kYellow = "\u001b[33m";
    const char *kBlue = "\u001b[34m";
    const char *kMagenta = "\u001b[35m";
    const char *kCyan = "\u001b[36m";
    const char *kWhite = "\u001b[37m";
    const char *kReset = "\u001b[0m";
}

SubstrateClient::SubstrateClient(std::shared_ptr<WsClient> ws_client, std::shared_ptr<jsonrpc::Client> jrpc_client) :
        ws_client{ws_client},
        jrpc_client{jrpc_client} {
    BOOST_ASSERT(this->ws_client != nullptr);
    BOOST_ASSERT(this->jrpc_client != nullptr);
    ws_client->handshake();
}

Metadata SubstrateClient::get_metadata() const {
    std::shared_ptr<jsonrpc::FormattedData> getMetadataRequest = jrpc_client->BuildRequestData("state_getMetadata");
    ws_client->write(getMetadataRequest->GetData());

    auto const response = ws_client->read();

    jsonrpc::Response parsedResponse = jrpc_client->ParseResponse(
            beast::buffers_to_string(response.data()));
    auto metadata_hex = parsedResponse.GetResult().AsString();
    auto metadata_bytes = kagome::common::unhexWith0x(metadata_hex).value();

    auto metadata = kagome::scale::decode<Metadata>(metadata_bytes).value();
    return metadata;
}

BlockHash SubstrateClient::get_genesis_hash() const {
    std::shared_ptr<jsonrpc::FormattedData> getGenesisHashRequest = jrpc_client->BuildRequestData("chain_getBlockHash",
                                                                                                  {0});
    ws_client->write(getGenesisHashRequest->GetData());
    auto const genesis_hash_response = ws_client->read();
    jsonrpc::Response parsedGenesisHashResponse = jrpc_client->ParseResponse(
            beast::buffers_to_string(genesis_hash_response.data()));
    auto genesis_hash_hex = parsedGenesisHashResponse.GetResult().AsString();
    auto genesis_hash_bytes = kagome::common::unhexWith0x(genesis_hash_hex).value();
    std::array<uint8_t, 32> genesis_hash{};
    std::copy_n(genesis_hash_bytes.begin(), 32, genesis_hash.begin());
    return genesis_hash;
}

Nonce SubstrateClient::get_nonce() const {
    std::shared_ptr<jsonrpc::FormattedData> getAccountNextIndexRequest = jrpc_client->BuildRequestData(
            "system_accountNextIndex",
            {"5GrwvaEF5zXb26Fz9rcQpDWS57CtERHpNehXCPcNoHGKutQY"});
    ws_client->write(getAccountNextIndexRequest->GetData());
    auto const nonce_response = ws_client->read();
    jsonrpc::Response parsedResponse = jrpc_client->ParseResponse(
            beast::buffers_to_string(nonce_response.data()));
    return parsedResponse.GetResult().AsInteger32();
}

void SubstrateClient::submit_encoded_extrinsic(std::vector<uint8_t> enc_ext) const {
    auto metadata = get_metadata();

    auto enc_enc_ext = kagome::scale::encode(enc_ext).value();
    auto ext_hex = kagome::common::hex_lower_0x(enc_enc_ext);

    std::shared_ptr<jsonrpc::FormattedData> submitExtrinsicRequest = jrpc_client->BuildRequestData(
            "author_submitExtrinsic",
            {ext_hex});

    ws_client->write(submitExtrinsicRequest->GetData());
    auto const response = ws_client->read();
    auto response_text = beast::buffers_to_string(response.data());
    try {
        jsonrpc::Response parsedResponse = jrpc_client->ParseResponse(
                response_text);
        std::cout << "RESPONSE: " << parsedResponse.GetResult() << "\n";
    } catch (jsonrpc::Fault &f) {
        std::cout << "FAULT: " << f.GetString() << " - " << response_text << "\n";
    }
}

void SubstrateClient::submit_unsigned_extrinsic(std::vector<uint8_t> encoded_call, MultiAddress address) const {

}

void SubstrateClient::submit_signed_extrinsic(std::vector<uint8_t> enc_call, MultiAddress address) const {
    auto metadata = get_metadata();
    auto genesis_hash = get_genesis_hash();
    std::cout << "genesis: " << kagome::common::hex_lower(genesis_hash) << "\n";
    auto nonce = get_nonce();
    std::cout << "nonce: " << nonce << "\n";
    SignedExtra extras{
            std::make_tuple(
                    CheckSpecVersion{
                            .spec_version = 100
                    },
                    CheckTxVersion{
                            .format_version = 1
                    },
                    CheckGenesis{
                            .genesis_hash = genesis_hash
                    },
                    CheckMortality{
                            .mortality = Immortal{},
                            .block_hash = genesis_hash
                    },
                    CheckNonce{
                            .nonce = nonce
                    },
                    CheckWeight{},
                    ChargeTransactionPayment{
                            .tip = 0
                    }
            )
    };
    auto extra = std::make_tuple(static_cast<uint8_t>(0), kagome::scale::CompactInteger{nonce}, kagome::scale::CompactInteger{0});
    auto additional_extra = std::make_tuple<uint32_t, uint32_t, std::array<uint8_t, 32> &, std::array<uint8_t, 32> &>(
            100, 1, genesis_hash, genesis_hash);
    std::vector<uint8_t> raw_payload;
    {
        auto e = kagome::scale::encode(std::make_tuple(extra, additional_extra)).value();
        raw_payload.resize(enc_call.size() + e.size());
        std::copy_n(enc_call.begin(), enc_call.size(), raw_payload.begin());
        std::copy_n(e.begin(), e.size(), raw_payload.begin() + enc_call.size());
    }
    std::cout << "raw payload: " << kRed << "call " << kReset << "extras: (" << kGreen << "mortality " << kYellow << "nonce "
    << kBlue << "tip" << kReset << ") additional: (" << kMagenta << "spec_ver " << kCyan << "format_ver "
    << kBlack << "genesis " << kGreen << "mortality_block" << kReset << ")\n";
    std::cout << "raw payload: "
              << kRed << kagome::common::hex_lower(enc_call)
              << kGreen << "00"
              << kYellow << kagome::common::hex_lower(kagome::scale::encode(kagome::scale::CompactInteger{nonce}).value())
              << kBlue << kagome::common::hex_lower(kagome::scale::encode(kagome::scale::CompactInteger{0}).value())
              << kMagenta << kagome::common::int_to_hex(100, 8)
              << kCyan << kagome::common::int_to_hex(1, 8)
              << kBlack << kagome::common::hex_lower(genesis_hash)
              << kGreen << kagome::common::hex_lower(genesis_hash)
              << kReset << "\n";
    std::cout << "raw payload: " << kagome::common::hex_lower(raw_payload) << "\n";
    std::vector<uint8_t> payload;
    if (raw_payload.size() > 256) {
        payload.resize(32);
        blake2b(payload.data(), 32, nullptr, 0, raw_payload.data(), raw_payload.size());
    } else {
        payload = std::move(raw_payload);
    }

    auto signature = MultiSignature{sign_extrinsic(ALICE_SEED, payload)};

    auto enc_ext = kagome::scale::encode(static_cast<uint8_t>(0b1000'0100),
                                         std::make_tuple(address, signature, extras)).value();
    enc_ext.insert(enc_ext.end(), enc_call.begin(), enc_call.end());

    std::cout << "version " << kRed << "address " << kGreen << "signature " << kYellow << "extra " << kBlue << "call\n"
              << kReset;
    std::cout << kagome::common::hex_lower(kagome::scale::encode(static_cast<uint8_t>(0b1000'0100)).value());
    std::cout << kRed << kagome::common::hex_lower(kagome::scale::encode(address).value());
    std::cout << kGreen << kagome::common::hex_lower(kagome::scale::encode(signature).value());
    std::cout << kYellow << kagome::common::hex_lower(kagome::scale::encode(extras).value());
    std::cout << kBlue << kagome::common::hex_lower(enc_call);
    std::cout << kReset << "\n";
    std::cout << kagome::common::hex_lower(enc_ext) << "\n";
    submit_encoded_extrinsic(enc_ext);
}

Sr25519Signature
SubstrateClient::sign_extrinsic(std::array<uint8_t, SR25519_SEED_SIZE> seed, std::vector<uint8_t> payload) const {
    std::array<uint8_t, SR25519_KEYPAIR_SIZE> keypair{};
    sr25519_keypair_from_seed(keypair.data(), seed.data());

    Sr25519Signature signature{};
    sr25519_sign(signature.data.data(), keypair.data() + SR25519_SECRET_SIZE, keypair.data(), payload.data(),
                 payload.size());
    return signature;
}

uint8_t SubstrateClient::get_module_idx(std::string_view module_name) const {
    auto metadata = get_metadata();
    auto module_it = std::find_if(metadata.modules_metadata.begin(), metadata.modules_metadata.end(),
                                  [module_name](auto &m) {
                                      return m.name == module_name;
                                  });
    if (module_it == std::end(metadata.modules_metadata)) {
        return 0xFF;
    }
    return std::distance(metadata.modules_metadata.begin(), module_it);
}

uint8_t SubstrateClient::get_call_idx(std::string_view module_name, std::string_view call_name) const {
    auto metadata = get_metadata();
    auto module_it = std::find_if(metadata.modules_metadata.begin(), metadata.modules_metadata.end(),
                                  [module_name](auto &m) {
                                      return m.name == module_name;
                                  });
    auto calls = module_it->calls.value();
    auto call_it = std::find_if(calls.begin(), calls.end(), [call_name](auto &c) {
        return c.name == call_name;
    });
    if (call_it == std::end(calls)) {
        return 0xFF;
    }
    return std::distance(calls.begin(), call_it);
}

std::string SubstrateClient::get_actor(uint32_t id) const {
    std::shared_ptr<jsonrpc::FormattedData> request = jrpc_client->BuildRequestData(
            "gameplay_getActor",
            {0});
    ws_client->write(request->GetData());
    auto const response = ws_client->read();
    jsonrpc::Response parsedResponse = jrpc_client->ParseResponse(
            beast::buffers_to_string(response.data()));
    return parsedResponse.GetResult().AsString();
}
