//
// Created by Harrm on 07.03.2021.
//

#ifndef THESISCPPCLIENT_WS_CLIENT_H
#define THESISCPPCLIENT_WS_CLIENT_H

#include <string_view>

#include <boost/beast/core.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/ip/tcp.hpp>

namespace beast = boost::beast;         // from <boost/beast.hpp>
namespace http = beast::http;           // from <boost/beast/http.hpp>
namespace websocket = beast::websocket; // from <boost/beast/websocket.hpp>
namespace net = boost::asio;            // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>

class WsClient {
public:

    WsClient(std::string_view hostname,
             std::string_view port) :
            hostname{hostname},
            ioc{},
            ws_stream{ioc} {

        tcp::resolver resolver{ioc};
        auto const results = resolver.resolve(hostname, port);
        auto endpoint = net::connect(ws_stream.next_layer(), results);
        this->hostname += ':' + std::to_string(endpoint.port());

        // Update the host_ string. This will provide the value of the
        // Host HTTP header during the WebSocket handshake.
        // See https://tools.ietf.org/html/rfc7230#section-5.4
        ws_stream.
                set_option(websocket::stream_base::decorator(
                [](websocket::request_type &req) {
                    req.set(http::field::user_agent,
                            std::string(BOOST_BEAST_VERSION_STRING) +
                            " websocket-client-coro");
                })
        );

    }

    void handshake() {
        ws_stream.handshake(hostname, "/");
    }

    void write(std::string_view text) {
        ws_stream.write(net::buffer(std::string(text)));
    }

    beast::flat_buffer read() {
        beast::flat_buffer buffer;
        ws_stream.read(buffer);
        return buffer;
    }

    void close() {
        ws_stream.close(websocket::close_code::normal);
    }

private:
    std::string hostname;
    net::io_context ioc;
    websocket::stream<tcp::socket> ws_stream;
};


#endif //THESISCPPCLIENT_WS_CLIENT_H
