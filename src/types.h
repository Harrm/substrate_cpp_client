//
// Created by Harrm on 07.03.2021.
//

#ifndef THESISCPPCLIENT_TYPES_H
#define THESISCPPCLIENT_TYPES_H

#include <scale/scale.hpp>
#include <scale/buffer/hexutil.hpp>

extern "C" {
#include <schnorrkel/schnorrkel.h>
#include <blake2b.h>
}

using Hash256 = std::array<uint8_t, 32>;
using BlockHash = Hash256;

enum StorageEntryModifier {
    Optional,
    Default
};

enum StorageHasher {
    Blake2_128,
    Blake2_256,
    Blake2_128Concat,
    Twox128,
    Twox256,
    Twox64Concat,
    Identity
};

using PlainStorageEntry = std::string;
struct MapStorageEntry {
    StorageHasher hasher;
    std::string key;
    std::string value;
    bool unused;
};

template<class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
Stream &operator>>(Stream &s, MapStorageEntry &meta) {
    uint8_t hasher{};
    s >> hasher >> meta.key >> meta.value >> meta.unused;
    meta.hasher = (StorageHasher) hasher;
    return s;
}

struct DoubleMapStorageEntry {
    StorageHasher hasher1;
    std::string key1;
    std::string key2;
    std::string value;
    StorageHasher hasher2;
};

template<class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
Stream &operator>>(Stream &s, DoubleMapStorageEntry &meta) {
    uint8_t hasher{}, hasher2{};
    s >> hasher >> meta.key1 >> meta.key2 >> meta.value >> hasher2;
    meta.hasher1 = (StorageHasher) hasher;
    meta.hasher2 = (StorageHasher) hasher2;
    return s;
}


using StorageEntryType = boost::variant<PlainStorageEntry, MapStorageEntry, DoubleMapStorageEntry>;


struct StorageEntryMetadata {
    std::string name;
    StorageEntryModifier modifier;
    StorageEntryType type;
    std::vector<uint8_t> value;
    std::vector<std::string> documentation;
};

template<class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
Stream &operator>>(Stream &s, StorageEntryMetadata &meta) {
    uint8_t modifier;
    s >> meta.name >> modifier >> meta.type >> meta.value >> meta.documentation;
    meta.modifier = (StorageEntryModifier) modifier;
    return s;
}

struct StorageMetadata {
    std::string prefix;
    std::vector<StorageEntryMetadata> entries;
};

template<class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
Stream &operator>>(Stream &s, StorageMetadata &meta) {
    s >> meta.prefix >> meta.entries;
    return s;
}

struct FunctionArgumentMetadata {
    std::string name;
    std::string type;
};

template<class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
Stream &operator>>(Stream &s, FunctionArgumentMetadata &meta) {
    s >> meta.name >> meta.type;
    return s;
}

struct FunctionMetadata {
    std::string name;
    std::vector<FunctionArgumentMetadata> arguments;
    std::vector<std::string> documentation;
};

template<class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
Stream &operator>>(Stream &s, FunctionMetadata &meta) {
    s >> meta.name >> meta.arguments >> meta.documentation;
    return s;
}

struct EventMetadata {
    std::string name;
    std::vector<std::string> arguments;
    std::vector<std::string> documentation;
};

template<class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
Stream &operator>>(Stream &s, EventMetadata &meta) {
    s >> meta.name >> meta.arguments >> meta.documentation;
    return s;
}

struct ModuleConstantMetadata {
    std::string name;
    std::string type;
    std::vector<uint8_t> value;
    std::vector<std::string> documentation;
};

template<class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
Stream &operator>>(Stream &s, ModuleConstantMetadata &meta) {
    s >> meta.name >> meta.type >> meta.value >> meta.documentation;
    return s;
}

struct ErrorMetadata {
    std::string name;
    std::vector<std::string> documentation;
};

template<class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
Stream &operator>>(Stream &s, ErrorMetadata &meta) {
    s >> meta.name >> meta.documentation;
    return s;
}

struct ModuleMetadata {
    std::string name;
    boost::optional<StorageMetadata> storage;
    boost::optional<std::vector<FunctionMetadata>> calls;
    boost::optional<std::vector<EventMetadata>> events;
    std::vector<ModuleConstantMetadata> constants;
    std::vector<ErrorMetadata> errors;
    uint8_t index;
};

template<class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
Stream &operator>>(Stream &s, ModuleMetadata &meta) {
    s >> meta.name >> meta.storage >> meta.calls >> meta.events >> meta.constants >> meta.errors >> meta.index;
    return s;
}

struct ExtrinsicMetadata {
    uint8_t version;
    std::vector<std::string> signed_extensions;
};

template<class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
Stream &operator>>(Stream &s, ExtrinsicMetadata &ext_meta) {
    s >> ext_meta.version >> ext_meta.signed_extensions;
    return s;
}

struct Metadata {
    static constexpr uint32_t kMagicNumber = 0x6174656D; // decoded "meta"
    uint8_t version;
    std::vector<ModuleMetadata> modules_metadata;
    ExtrinsicMetadata ext_metadata;
};

/**
 * @brief decodes buffer object from stream
 * @tparam Stream input stream type
 * @param s stream reference
 * @param buffer value to decode
 * @return reference to stream
 */
template<class Stream, typename = std::enable_if_t<Stream::is_decoder_stream>>
Stream &operator>>(Stream &s, Metadata &m) {
    uint32_t magic{};
    s >> magic >> m.version >> m.modules_metadata >> m.ext_metadata;
    BOOST_ASSERT(magic == m.kMagicNumber);
    //std::cout << "MAGIC: " << magic << "\n";
    return s;
}

using AccountIndex = kagome::scale::CompactInteger;

using AccountId = std::array<uint8_t, 32>;
using RawAddress = std::vector<uint8_t>;
using Address32 = std::array<uint8_t, 32>;
using Address20 = std::array<uint8_t, 20>;
using MultiAddress = boost::variant<AccountId, /*compact codec*/AccountIndex, RawAddress, Address32, Address20>;

const AccountId ALICE_ACCOUNT_ID = []() {
    auto bytes = kagome::common::unhex("d43593c715fdd31c61141abd04a99fd6822c8558854ccde39a5684e7a56da27d").value();
    BOOST_ASSERT(bytes.size() == SR25519_PUBLIC_SIZE);
    AccountId id;
    std::copy_n(bytes.begin(), id.size(), id.begin());
    return id;
}();

struct Ed25519Signature {
    std::array<uint8_t, ED25519_SIGNATURE_LENGTH> data;
};

template<class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream &operator<<(Stream &s, const Ed25519Signature &sig) {
    return s << sig.data;
}

struct Sr25519Signature {
    std::array<uint8_t, SR25519_SIGNATURE_SIZE> data;
};

template<class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream &operator<<(Stream &s, const Sr25519Signature &sig) {
    return s << sig.data;
}

struct EcdsaSignature {
    std::array<uint8_t, 65> data;
};

template<class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream &operator<<(Stream &s, const EcdsaSignature &sig) {
    return s << sig.data;
}

using MultiSignature = boost::variant<Ed25519Signature, Sr25519Signature, EcdsaSignature>;

const std::array<uint8_t, SR25519_SEED_SIZE> ALICE_SEED = []() {
    auto bytes = kagome::common::unhex("e5be9a5092b81bca64be81d212e7f2f9eba183bb7a90954f7b76361f6edb5c0a").value();
    BOOST_ASSERT(bytes.size() == SR25519_SEED_SIZE);
    std::array<uint8_t, SR25519_SEED_SIZE> secret{};
    std::copy_n(bytes.begin(), secret.size(), secret.begin());
    return secret;
}();

struct CheckSpecVersion {
    uint32_t spec_version;
};

template<class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream &operator<<(Stream &s, const CheckSpecVersion &ext) {
    return s;
}

struct CheckTxVersion {
    uint32_t format_version;
};

template<class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream &operator<<(Stream &s, const CheckTxVersion &ext) {
    return s;
}

struct CheckGenesis {
    std::array<uint8_t, 32> genesis_hash;
};

template<class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream &operator<<(Stream &s, const CheckGenesis &ext) {
    return s;
}

struct Immortal {
};

template<class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream &operator<<(Stream &s, const Immortal &m) {
    return s;
}

struct Mortal {
    uint64_t period;
    uint64_t phase;
};

template<class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream &operator<<(Stream &s, const Mortal &m) {
    return s;
}

using Era = boost::variant<Immortal, Mortal>;

struct CheckMortality {
    Era mortality;
    std::array<uint8_t, 32> block_hash;
};

template<class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream &operator<<(Stream &s, const CheckMortality &ext) {
    return s << ext.mortality;
}

using Nonce = kagome::scale::CompactInteger; // uint32_t; // transaction index
struct CheckNonce {
    Nonce nonce; // compact codec
};

template<class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream &operator<<(Stream &s, const CheckNonce &ext) {
    return s << ext.nonce;
}

struct CheckWeight {
};

template<class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream &operator<<(Stream &s, const CheckWeight &ext) {
    return s;
}

using Balance = kagome::scale::CompactInteger; // boost::multiprecision::uint128_t;
struct ChargeTransactionPayment {
    Balance tip; // compact codec
};

template<class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>>
Stream &operator<<(Stream &s, const ChargeTransactionPayment &ext) {
    return s << ext.tip;
}

using SignedExtra = std::tuple<
        CheckSpecVersion,
        CheckTxVersion,
        CheckGenesis,
        CheckMortality,
        CheckNonce,
        CheckWeight,
        ChargeTransactionPayment>;

template<typename... Args>
struct Call {
    uint8_t module;
    uint8_t call;
    std::tuple<Args...> args;
};

template<class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>, typename... Args>
Stream &operator<<(Stream &s, const Call<Args...> &call) {
    return s << call.module << call.call << call.args;
}

template<typename... Args>
struct SignedExtrinsic {
    uint8_t type_version = 0b1000'0100; // 0... - unsigned, 1... - signed, ... - version (latest is 4 at the moment of writing this code)
    std::tuple<MultiAddress, MultiSignature, SignedExtra> signature;
    Call<Args...> function;
};

template<typename... Args>
struct UnsignedExtrinsic {
    uint8_t type_version = 0b0000'0100; // 0... - unsigned, 1... - signed, ... - version (latest is 4 at the moment of writing this code)
    Call<Args...> function;
};

template<class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>, typename... Args>
Stream &operator<<(Stream &s, const SignedExtrinsic<Args...> &ext) {
    return s << ext.type_version << ext.signature << ext.function;
}

template<class Stream, typename = std::enable_if_t<Stream::is_encoder_stream>, typename... Args>
Stream &operator<<(Stream &s, const UnsignedExtrinsic<Args...> &ext) {
    return s << ext.type_version << ext.function;
}


template<typename... Args>
SignedExtrinsic<Args...>
create_signed(uint8_t module, uint8_t call_id, std::array<uint8_t, 32> genesis_hash, Nonce nonce, Args &&... args) {
    MultiAddress address = ALICE_ACCOUNT_ID;
    Call call{
            module, call_id, std::make_tuple(std::forward<Args>(args)...)
    };

    SignedExtra extras{
            std::make_tuple(
                    CheckSpecVersion{
                            .spec_version = 100
                    },
                    CheckTxVersion{
                            .format_version = 1
                    },
                    CheckGenesis{
                            .genesis_hash = genesis_hash
                    },
                    CheckMortality{
                            .mortality = Immortal{},
                            .block_hash = genesis_hash
                    },
                    CheckNonce{
                            .nonce = nonce
                    },
                    CheckWeight{},
                    ChargeTransactionPayment{
                            .tip = 0
                    }
            )
    };
    auto extra = std::make_tuple<uint8_t, kagome::scale::CompactInteger &, kagome::scale::CompactInteger>(0, nonce, 0);
    auto additional_extra = std::make_tuple<uint32_t, uint32_t, std::array<uint8_t, 32> &, std::array<uint8_t, 32> &>(
            100, 1, genesis_hash, genesis_hash);
    auto raw_payload = kagome::scale::encode(std::make_tuple(call, extra, additional_extra)).value();
    std::vector<uint8_t> payload;
    if (raw_payload.size() > 256) {
        payload.resize(32);
        blake2b(payload.data(), 32, nullptr, 0, raw_payload.data(), raw_payload.size());
    } else {
        payload = std::move(raw_payload);
    }
    std::cout << "payload: " << kagome::common::hex_lower(payload) << "\n";

    std::array<uint8_t, SR25519_KEYPAIR_SIZE> keypair{};
    sr25519_keypair_from_seed(keypair.data(), ALICE_SEED.data());

    Sr25519Signature signature{};
    sr25519_sign(signature.data.data(), ALICE_ACCOUNT_ID.data(), keypair.data(), payload.data(), payload.size());
    std::cout << "secret: " << kagome::common::hex_lower(keypair).substr(0, 64) << "\n";
    std::cout << "signature: " << kagome::common::hex_lower(signature.data) << "\n";

    SignedExtrinsic<Args...> ext{
            .type_version = 0b1000'0100,
            .signature = std::make_tuple(address, signature, extras),
            .function = call
    };
    return ext;
}

template<typename... Args>
UnsignedExtrinsic<Args...> create_unsigned(uint8_t module, uint8_t call_id, Args &&... args) {
    Call call{
            module, call_id, std::make_tuple(std::forward<Args>(args)...)
    };
    UnsignedExtrinsic<Args...> ext{
            .function = call
    };

    return ext;
}

#endif //THESISCPPCLIENT_TYPES_H
