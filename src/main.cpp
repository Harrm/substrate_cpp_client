#include <iostream>
#include <string>

#include <jsonrpc-lean/client.h>

#include "substrate_client.h"
#include "ws_client.h"

std::string level(size_t level) {
    return std::string(level * 4, ' ');
}

static size_t current_level = 0;

decltype(auto) open() {
    ++current_level;
}

decltype(auto) close() {
    --current_level;
}

decltype(auto) indent() {
    return std::cout << level(current_level);
}

void print_metadata(Metadata const &meta) {
    indent() << "{\n";
    open();
    indent() << "'version': " << (int) meta.version << ",\n";
    indent() << "'modules metadata': [\n";
    for (auto &module: meta.modules_metadata) {
        open();
        indent() << "{\n";
        open();
        indent() << "'name': " << module.name << ",\n";
        // storage
        // calls
        if (module.calls) {
            indent() << "'calls': [\n";
            for (auto &call: module.calls.value()) {
                open();
                indent() << "{\n";
                open();
                indent() << "'name': " << call.name << ",\n";
                indent() << "'args': [";
                for (auto &arg: call.arguments) {
                    std::cout << arg.type << " " << arg.name << ", ";
                }
                std::cout << "],\n";
                indent() << "'docs':[\n";
                open();
                for (auto &str: call.documentation) {
                    indent() << "'" << str << "',\n";
                }
                close();
                indent() << "]\n";
                close();
                indent() << "},\n";
                close();
            }

        } else {
            indent() << "'calls': None,\n";
        }
        // events
        // constants
        // errors

        // index
        indent() << "'index': " << (int) module.index << "\n";

        close();
        close();
    }
    indent() << "]\n";
    close();
    open();
    indent() << "'extrinsic metadata': {\n";
    open();
    indent() << "'version': " << (int) meta.ext_metadata.version << "\n";
    indent() << "'extensions': [";
    for (auto &ext: meta.ext_metadata.signed_extensions) {
        std::cout << "'" << ext << "', ";
    }
    std::cout << "]\n";
    close();
    indent() << "\t}\n";
    indent() << "}\n";
    
}

// Sends a WebSocket message and prints the response
int main(int argc, char **argv) {
    HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
    if (hOut == INVALID_HANDLE_VALUE)
    {
        return GetLastError();
    }

    DWORD dwMode = 0;
    if (!GetConsoleMode(hOut, &dwMode))
    {
        return GetLastError();
    }

    dwMode |= ENABLE_VIRTUAL_TERMINAL_PROCESSING;
    if (!SetConsoleMode(hOut, dwMode))
    {
        return GetLastError();
    }

    std::string host = "localhost";
    auto const port = "9944";

    std::unique_ptr<jsonrpc::FormatHandler> formatHandler(new jsonrpc::JsonFormatHandler());
    auto jrpc_client = std::make_shared<jsonrpc::Client>(*formatHandler);

    try {
        auto ws_client = std::make_shared<WsClient>(host, port);
        SubstrateClient client(ws_client, jrpc_client);

        print_metadata(client.get_metadata());

        AccountId deadbeef_id{0xDE, 0xAD, 0xBE, 0xEF};
        AccountId charlie_id;
        {
            auto bytes = kagome::common::unhexWith0x(
                    "0x90b5ab205c6974c9ea841be688864633dc9ca8a357843eeacf2314649965fe22").value();
            std::copy_n(bytes.begin(), charlie_id.size(), charlie_id.begin());
        }
        Call<MultiAddress, kagome::scale::CompactInteger> call{
                client.get_module_idx("Balances"),
                client.get_call_idx("Balances", "transfer"),
                std::make_tuple(charlie_id, 100500)
        };
        std::cout << (int)client.get_module_idx("Gameplay") << " " << (int)client.get_call_idx("Gameplay", "spawn_actor") << "\n";
        Call<uint32_t, std::string> spawn_actor_call{
                client.get_module_idx("Gameplay"),
                client.get_call_idx("Gameplay", "spawn_actor"),
                std::tuple{0, "Actor1"}
        };
//
        auto enc_spawn_call = kagome::scale::encode(spawn_actor_call).value();
        auto enc_transfer_call = kagome::scale::encode(call).value();
        //client.submit_signed_extrinsic(enc_transfer_call, ALICE_ACCOUNT_ID);
        client.submit_signed_extrinsic(enc_spawn_call, ALICE_ACCOUNT_ID);
        client.submit_signed_extrinsic(enc_spawn_call, ALICE_ACCOUNT_ID);
        client.submit_signed_extrinsic(enc_spawn_call, ALICE_ACCOUNT_ID);

        ws_client->close();

    } catch (std::exception const &e) {
        std::cerr << "Error: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
