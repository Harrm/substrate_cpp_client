//
// Created by Harrm on 14.01.2021.
//

#ifndef SCALE_OUTCOME_OUTCOME
#define SCALE_OUTCOME_OUTCOME

#include <boost/outcome/std_result.hpp>
#include <boost/outcome/success_failure.hpp>
#include <boost/outcome/try.hpp>

#include "outcome-register.hpp"

namespace outcome {

using namespace BOOST_OUTCOME_V2_NAMESPACE;

template <class R, class S = std::error_code,
          class NoValuePolicy = policy::default_policy<R, S, void>>
using result = basic_result<R, S, NoValuePolicy>;

} // namespace outcome

#define OUTCOME_TRY(...) BOOST_OUTCOME_TRY(__VA_ARGS__)

#endif // SCALE_OUTCOME_OUTCOME
