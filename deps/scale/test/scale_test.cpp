//
// Created by Harrm on 15.01.2021.
//

#include <cassert>

#include <scale/scale.hpp>

int main() {
  auto enc = kagome::scale::encode(42).value();
  auto dec = kagome::scale::decode<int>(enc).value();
  assert(42 == dec);
}
